### Balises (Tags) Part 2
- <mark style="background: #FF5582A6;">Sauts de ligne</mark> (`<br></br>`)
- <mark style="background: #FF5582A6;">Séparation horizontales</mark> (`<hr> </hr>`)
<br><br>

<pre><code><script type="text/template">
    <hr><p>du texte<br>plus de texte<hr>encore plus de texte</p>
</script></code></pre>
<hr><p>du texte<br>plus de texte<hr>encore plus de texte</p>
