### Lien Mailto
Le lien "mailto" permet d'ouvrir le client de messagerie par défaut de l'utilisateur avec une adresse e-mail préremplie.
<hr>
<pre><code><script type="text/template"><a href="mailto:kmichoud@unistra.fr">Contactez-moi</a></script></code></pre>
<a href="mailto:kmichoud@unistra.fr">Contactez-moi</a>