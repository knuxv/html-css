### Lien relatif
<p>Un lien relatif permet de pointer vers une autre page du même site web. Il est basé sur la <mark>position actuelle</mark> de la page, et non sur la racine du site.</p>
<pre><code>
&lt;a href="pages/contact.html"&gt;Accéder à la page de contact&lt;/a&gt;
</code></pre>
<p>Dans cet exemple, le lien dirige vers la page de contact située dans le dossier "pages".</p>