<section data-markdown>    
<pre><code><script type="text/template">
<h1>C'est le Titre Principal</h1>
<p>
    Ce texte pourrait être une introduction au reste de
    la page.
</p>
<h2>C'est un Sous-Titre</h2>
<p>
    De nombreux longs articles ont des sous-titres pour aider
    à suivre la structure de ce qui est écrit.
</p>
<h4>Un Autre Sous-sous-Titre</h4>
<p>
    Ici, vous pouvez voir un autre sous-titre.
</p>
</script></code></pre>
</section>
<section>
        <h2>Titre Principal</h2>
        <p>
            Ce texte pourrait être une introduction au reste de
            la page.
        </p>
        <h3>C'est un Sous-Titre</h3>
        <p>
            De nombreux longs articles ont des sous-titres pour aider
            à suivre la structure de ce qui est écrit.
        </p>
        <h4>Un Autre Sous-sous-Titre</h4>
        <p>
            Ici, vous pouvez voir un autre sous-titre.
        </p>
</section>
