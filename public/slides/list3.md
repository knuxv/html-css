### Listes imbriquées / Sous liste
Il est possible de mettre une seconde liste dans un élément `<li>` pour créer une sous-liste.
<hr>
<section data-markdown="list.md" data-separator="^(\r?\n){2,}" data-vertical="^(\r?\n){1,}">
    <div style="width: 60%; float: left;">
        <pre><code><script type="text/template">
<ol>
    <li>Légumes</li>
    <li>Yahourts</li>
    <li>Fruits</li>
    <li>Viennoiserie
        <ul>
        <li>Croissant</li>
        <li>Mille-feuille</li>
        <li>Palmier</li>
        <li>Chocolatine</li>
        </ul>
    </li>
</ol>
        </script></code></pre>
    </div>
    <div style="width: 40%; float: right;">
        <!-- Your display code here -->
        <ol>
            <li>Légumes</li>
            <li>Yahourts</li>
            <li>Fruits</li>
            <li>Viennoiserie
                <ul>
                <li>Croissant</li>
                <li>Mille-feuille</li>
                <li>Palmier</li>
                <li>Chocolatine</li>
                </ul>
            </li>
        </ol>
    </div>
</section>
