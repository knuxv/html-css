<div>
    <h3>Listes ordonnées</h3>
    <ul>
        <li><mark style="background: #FF5582A6;">Liste ordonnée</mark> (&lt;ol&gt; &lt;/ol&gt;)</li>
        <li><mark style="background: #FF5582A6;">Élément d'une liste</mark> (&lt;li&gt; &lt;/li&gt;)</li>
    </ul>
    </br>
</div>
<br><hr></br>
<div>
<div style="width: 50%; float: left;">
<pre><code><script type="text/template">
    <h3>Liste de course</h3>
    <ol>
        <li>Légumes</li>
        <li>Yahourts</li>
        <li>Fruits</li>
    </ol>
</script></code></pre>
</div>
<div style="width: 50%; float: right;">
<h3>Liste de course</h3>
<ol>
    <li>Légumes</li>
    <li>Yahourts</li>
    <li>Fruits</li>
</ol>
</div>
</div>