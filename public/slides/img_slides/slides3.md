### Images <sup>Partie 3</sup> 
#### Hauteur-Largeur
<hr>
<div>
    <pre><code>
&lt;img src="img/GeekforGeeks.png" alt="geeksforgeeks" width="251" height="141"/&gt;
    </code></pre>
    <p>
        Les attributs <code>width</code> et <code>height</code> permettent de définir la taille de l'image.<br>
        Ici, la largeur est fixée à 251 pixels et la hauteur à 141 pixels.
    </p>
    <img src="/img/GeekforGeeks.png" alt="geeksforgeeks" width="251" height="141"/>
</div>
