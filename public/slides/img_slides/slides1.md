### Images
- <mark style="background: #FF5582A6;">`<img>`</mark> permet d'intégrer des images dans une page web. 
- <u>Balise auto-fermante</u>
- Accepte les formats d'image tels que <u>JPEG</u>, <u>PNG</u>, <u>GIF</u>, etc.
- 2 attributs nécéssaires 'src' et 'alt'
    - 'src' contient le chemin de l'image
    - 'alt' contient une description de l'image
<hr>
<pre><code><script type="text/template"><img src="img/chemin.png" alt="nom alternatif"/></script></code></pre>
