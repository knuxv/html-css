### Images <sup>Partie 2</sup>
<div style="display: flex; flex-direction: column;">
    <div style="display: flex; align-items: center; justify-content: space-between;">
        <h5 style="margin: 0;">Bon Chemin</h5>
        <pre style="display: inline; margin: 0;"><code>
&lt;img src="../img/GeeksforGeeks.png" alt="geeksforgeeks" /&gt;
        </code></pre>
    </div>
    <div>
    <img src="../../img/GeekforGeeks.png" alt="geeksforgeeks" width="251" height="141"/>
    </div>
    <div style="display: flex; align-items: center; justify-content: space-between;">
        <h6 style="margin: 0; margin-right: 20px">Mauvais Chemin</h6>
        <pre style="display: inline; margin: 0;"><code>
&lt;img src="img_mauvais_chemin.png" alt="Image manquante" /&gt;
        </code></pre>
    </div>
    <div>
    <img src="img_mauvais_chemin.png" alt="Image manquante" />
    </div>
</div>
