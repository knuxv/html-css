<div>
    <h3>Listes non ordonnées</h3>
    <ul>
        <li><mark style="background: #FF5582A6;">Liste non ordonnée</mark> (&lt;ul&gt; &lt;/ul&gt;)</li>
        <li><mark style="background: #FF5582A6;">Élément d'une liste</mark> (&lt;li&gt; &lt;/li&gt;)</li>
    </ul>
    <br>
</div>
<br><hr><br>
<div>
    <div style="width: 40%; float: left;">
        <pre><code><script type="text/template">
<h3>Liste de course</h3>
<ul>
    <li>Légumes</li>
    <li>Yahourts</li>
    <li>Fruits</li>
</ul>
        </script></code></pre>
    </div>
    <div style="width: 60%; float: right;">
        <h3>Liste de course</h3>
        <ul>
            <li>Légumes</li>
            <li>Yahourts</li>
            <li>Fruits</li>
        </ul>
    </div>
</div>
