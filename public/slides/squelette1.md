### Squelette de votre fichier HTML

- <mark style="background: #FF5582A6;">`<!DOCTYPE html>`</mark> : Tous les fichiers HTML doivent commencer par un DOCTYPE.

- <mark style="background: #FF5582A6;">`<html> </html>`</mark> : contient tous les autres éléments.

- <mark style="background: #FF5582A6;">`<head> </head>`</mark> : contient les métadonnées du documents dont `title` et `meta`.

- <mark style="background: #FF5582A6;">`<body> </body>`</mark> : contient le contenu de la page HTML
