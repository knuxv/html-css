<div style="display: flex; align-items: flex-start; justify-content: space-between; margin-top: 0;">
    <div style="width: 65%;">
        <img src="img/img_ex3.png" alt="Image of Exercise 3">
    </div>
    <div style="width: 35%;">
        <h4><strong>Exercice 3</strong> : Insertion images</h4>
        <hr>
        <div style="font-size: 0.6em;">
            <p>Insérez les images que vous aurez récupérées sur Moodle</p>
            <ul>
            <li>plums.jpg</li>
            <li>redwheel.jpg</li> 
            </ul>
            <p> Placez celle des prunes sous le premier poème, puis redwheel sous le deuxième poème.</p>
            <p><strong>Note :</strong> Contrairement à l'image de gauche, les poèmes sont toujours l'un sur l'autre.</p>
        </div>
    </div>
</div>
