<div style="display: flex; flex-direction: column; align-items: flex-start; justify-content: space-between; margin-top: 0;">
    <div style="display: flex; align-items: flex-start; justify-content: space-between; width: 100%;">
        <div style="width: 70%;">
            <img src="img/img_ex2.png" alt="Image of Exercise 2">
        </div>
        <div style="width: 30%;">
            <h4><strong>Exercice 2</strong> : Listes</h4>
            <hr>
        </div>
    </div>
    <div style="font-size: 0.7em; width: 100%;">
        <p>Sur la base du texte listes.txt (dans moodle), écrire le code HTML qui rend le contenu comme ci-dessus</p>
        <p><strong>Note :</strong> le titre de la fenêtre sera « Listes de nourriture (voir William Carlos Williams) »</p>
    </div>
</div>
