<section>
    <div style="display: flex; align-items: flex-start; justify-content: space-between; margin-top: 0;">
        <div style="width: 40%;">
            <img src="img/img_ex1.png" alt="Image of Exercise 1">
        </div>
        <div style="width: 55%;">
            <h4><strong>Exercice 1</strong> : Paragraphes, mise en valeur</h4>
            <hr>
            <div style="font-size: 0.7em;">
                <p>Sur la base du texte de deux poèmes par William Carlos Williams (à récupérer dans textes.txt sur moodle): 
                </p>
                <p>Créez un nouveau fichier html `poeme.html`, qui affiche le texte des poèmes comme sur l'image de droite </p>
                <p><strong>Note :</strong> le titre de la page sera « Deux poèmes par William Carlos Williams »
                </p>
            </div>
        </div>
    </div>
</section>
