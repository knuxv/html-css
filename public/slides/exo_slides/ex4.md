<div style="display: flex; align-items: flex-start; justify-content: space-between; margin-top: 0;">
    <div style="width: 30%;">
        <img src="img/img_ex4.png" alt="Image of Exercise 4">
    </div>
    <div style="width:70%;">
    <h4><strong>Exercice 4</strong> : Hyperliens</h4>
    <hr>
    </div>
</div>
<div>
<ul style="font-size: 0.7em;">
    <li>Créez un dossier <u>site</u></li>
    <li>Dans <u>site</u>, créez 2 sous-dossiers <u>img</u> et <u>pages</u> ainsi qu'une page html <u>index.html</u></li>
    <li>Dans <u>img</u>, copiez les images plums.jpg et redwheel.jpg de l'exercice précédent</li>
    <li>Dans <u>pages</u>, créez des pages html vierges <u>depend.html</u> et <u>dire.html</u></li>
    <li><u>index.html</u> contient les liens vers <u>dire.html</u> et <u>depend.html</u></li>
    <li><u>dire.html</u> affiche l'image plums.jpg, <u>depend.html</u> affiche l'image redwheel.jpg</li>
</ul>
<p style="font-size: 0.7em;"><strong>Note :</strong> pour revenir au dossier parent, utilisez "../".<br> Si vous êtes dans "pages" et que vous voulez aller dans "img", donnez le chemin "../img/votre_page.html".</p>
</div>