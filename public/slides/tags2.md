### Autres Balises (Tags)
- <mark style="background: #FF5582A6;">Fort (Strong)</mark> (`<strong> </strong>`)
- <mark style="background: #FF5582A6;">Emphase (Emphasis)</mark> (`<em> </em>`)
- <mark style="background: #FF5582A6;">Citation</mark> (`<blockquote> </blockquote>`)
- <mark style="background: #FF5582A6;">Souligné (Underline)</mark> (`<u> </u>`)

<a href="https://fr.w3docs.com/apprendre-html/tableau-des-tags-html.html">Toutes les balises</a>
