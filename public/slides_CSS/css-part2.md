<h3>Combinaison CSS</h3>
<div style="width: 100%; font-size:70%;">
    <p>Un <strong>combinaison</strong> décrit la relation entre les sélecteurs.</p>
    <p>Un sélecteur CSS peut contenir plusieurs sélecteurs simples.</p>
    <strong>Il existe <u>4</u> combinaisons différents en CSS :</strong>
    <ul>
        <li><strong>Descendant</strong> (espace) - Correspond à tous les éléments qui sont des descendants d'un élément spécifié.</li>
        <li><strong>Enfant</strong> (>)  Sélectionne tous les éléments qui sont les enfants directs d'un élément spécifié.</li>
        <li><strong>Adjacent</strong> (+)  Sélectionne un élément qui se trouve immédiatement après un élément spécifique. Les éléments frères doivent avoir le même élément parent.</li>
        <li><strong>Général</strong> (~)  Sélectionne tous les éléments qui sont les frères suivants d'un élément spécifié.</li>
    </ul>
</div>



<h3>Descendant: div p</h3>
<!-- Left side: HTML & CSS code example -->
<div style="float: left; width: 50%; font-size:70%;">
    <strong>HTML</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<section>
    <h2>Sélecteur de Descendant</h2>
    <p>Le sélecteur de descendant correspond à tous les éléments qui sont des descendants d'un élément spécifié.</p>
    <div>
    <p>Paragraphe 1 dans le div.</p>
    <p>Paragraphe 2 dans le div.</p>
    <section><p>Paragraphe 3 dans le div.</p></section>
    </div>
    <p>Paragraphe 4. Pas dans un div.</p>
    <p>Paragraphe 5. Pas dans un div.</p>
</section>
    </script></code></pre>
    <br>
    <strong>CSS</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<style>
div p {
    background-color: yellow;
}
</style>
    </script></code></pre>
</div>

<!-- Right side: Display the result -->
<div style="float: right; width: 50%;">
    <strong style="font-size:70%;">Résultat :</strong>
    <div style="font-size:0.5em; font-family: Arial, sans-serif; background-color: lightgrey; margin: 10px;">
        <h2>Sélecteur de Descendant</h2>
        <p>Le sélecteur de descendant correspond à tous les éléments qui sont des descendants d'un élément spécifié.</p>
        <div>
            <p style="background-color: yellow;">Paragraphe 1 dans le div.</p>
            <p style="background-color: yellow;">Paragraphe 2 dans le div.</p>
            <section><p style="background-color: yellow;">Paragraphe 3 dans le div.</p></section>
        </div>
        <p>Paragraphe 4. Pas dans un div.</p>
        <p>Paragraphe 5. Pas dans un div.</p>
    </div>
</div>
<div style="clear:both;"></div>
<a href="https://www.w3schools.com/css/tryit.asp?filename=trycss_sel_element_element"> Exemple Descendant Enfant sur W3Schools </a>



<h3>Enfant: div > p</h3>
<!-- Left side: HTML & CSS code example -->
<div style="float: left; width: 50%; font-size:70%;">
    <strong>HTML</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<section>
    <h2>Sélecteur d'Enfant</h2>
    <p>Le sélecteur d'enfant sélectionne tous les éléments qui sont les enfants directs d'un élément spécifié.</p>
    <div>
        <p>Paragraphe 1 dans le div.</p>
        <section>
            <p>Paragraphe 2 dans la section.</p>
            <p>Paragraphe 3 dans la section.</p>
        </section>
    </div>
    <p>Paragraphe 4. Pas dans un div.</p>
</section>
    </script></code></pre>
    <br>
    <strong>CSS</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<style>
div > p {
    background-color: yellow;
}
</style>
    </script></code></pre>
</div>
<!-- Right side: Display the result -->
<div style="float: right; width: 50%;">
    <strong style="font-size:70%;">Résultat :</strong>
    <div style="font-size:0.5em; font-family: Arial, sans-serif; background-color: lightgrey; margin: 10px;">
        <h2>Sélecteur d'Enfant</h2>
        <p>Le sélecteur d'enfant sélectionne tous les éléments qui sont les enfants directs d'un élément spécifié.</p>
        <div>
            <p style="background-color: yellow;">Paragraphe 1 dans le div.</p>
            <section>
            <p>Paragraphe 2 dans la section.</p>
            <p>Paragraphe 3 dans la section.</p>
            </section>
        </div>
        <p>Paragraphe 4. Pas dans un div.</p>
    </div>
</div>
<div style="clear:both;"></div>
<a href="https://www.w3schools.com/css/tryit.asp?filename=trycss_sel_element_gt"> Exemple Selecteur Enfant sur W3Schools </a>



<h3>Adjacent: div + p</h3>
<!-- Left side: HTML & CSS code example -->
<div style="float: left; width: 50%; font-size:70%;">
    <strong>HTML</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<div>
    <h2>Sélecteur de Frère Adjacent</h2>
    <p>Le sélecteur de frère adjacent est utilisé pour sélectionner un élément qui est directement après un élément spécifique.</p>
    <div>Div sans paragraphe</div>
    <p>Paragraphe 1 immédiatement après le div.</p>
    <section>
        <p>Paragraphe 2 dans le section.</p>
    </section>
    <p>Paragraphe 3. Pas immédiatement après le div.</p>
</div>
    </script></code></pre>
    <br>
    <strong>CSS</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<style>
div + p {
    background-color: yellow;
}
</style>
    </script></code></pre>
</div>
<!-- Right side: Display the result -->
<div style="float: right; width: 50%;">
    <strong style="font-size:70%;">Résultat :</strong>
    <div style="font-size:0.5em; font-family: Arial, sans-serif; background-color: lightgrey; margin: 10px;">
        <h2>Sélecteur Adjacent</h2>
        <p>Le sélecteur adjacent est utilisé pour sélectionner un élément qui est directement après un élément spécifique.</p>
        <div>Div sans paragraphe</div>
        <p style="background-color: yellow;">Paragraphe 1 immédiatement après le div.</p>
        <section>
            <p>Paragraphe 2 dans le section.</p>
        </section>
        <p>Paragraphe 3. Pas immédiatement après le div.</p>
    </div>
</div>
<p><a href="https://www.w3schools.com/css/tryit.asp?filename=trycss_sel_element_pluss"> Exemple sur W3Schools </a></p>



<h3>Selecteur General: div ~ p</h3>
<div style="float: left; width: 50%; font-size:70%;">
    <strong>HTML</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<div>
    <h2>Sélecteur Général</h2>
    <p>Le sélecteur général sélectionne tous les éléments qui sont les frères suivants d'un élément spécifié.</p>
    <div>Div sans paragraphe</div>
    <p>Paragraphe 1 après le div.</p>
    <section>
        <p>Paragraphe 2 dans le section.</p>
    </section>
    <p>Paragraphe 3 également après le div.</p>
</div>
    </script></code></pre>
    <br>
    <strong>CSS</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<style>
div ~ p {
    background-color: yellow;
}
</style>
    </script></code></pre>
</div>
<!-- Right side: Display the result -->
<div style="float: right; width: 50%;">
    <strong style="font-size:70%;">Résultat :</strong>
    <div style="font-size:0.5em; font-family: Arial, sans-serif; background-color: lightgrey; margin: 10px;">
        <h2>Sélecteur général</h2>
        <p>Le sélecteur général sélectionne tous les éléments qui sont les frères suivants d'un élément spécifié.</p>
        <div>Div sans paragraphe</div>
        <p style="background-color: yellow;">Paragraphe 1 après le div.</p>
        <section>
            <p>Paragraphe 2 dans le section.</p>
        </section>
        <p style="background-color: yellow;">Paragraphe 3 également après le div.</p>
    </div>
</div>
<div style="clear:both;"></div>
<p><a href="https://www.w3schools.com/css/tryit.asp?filename=trycss_sel_element_tilde"> Exemple Selecteur General sur W3Schools</a></p>



<h3>Pseudo-classes CSS</h3>
<div style="width: 100%; font-size:70%;">
    <p>Les <u><a href="https://www.w3schools.com/css/css_pseudo_classes.asp">pseudo-classes</a></u> permettent de sélectionner des éléments en fonction de certaines caractéristiques qui ne peuvent pas être ciblées avec des sélecteurs simples.</p>
    <p>Elles sont précédées par un double-point (":").</p>
    <ul>
        <li><strong>:hover</strong> - Sélectionne un élément lorsqu'il est survolé par le curseur.</li>
        <li><strong>:active</strong> - Sélectionne un élément lorsqu'il est activé (par exemple, lorsqu'on clique sur un lien).</li>
        <li><strong>:focus</strong> - Sélectionne un élément lorsqu'il reçoit le focus (par exemple, lorsqu'on clique sur un champ de formulaire).</li>
        <li><strong>:visited</strong> - Sélectionne un lien qui a déjà été visité par l'utilisateur.</li>
        <li><strong>:first-child (:last-child)</strong> - Sélectionne un élément s'il est le premier (dernier) enfant de son parent.</li>
        <li><strong>:nth-child(n)</strong> - Sélectionne un élément en fonction de sa position parmi ses frères.</li>
        <li><strong>:first-of-type (:last-of-type)</strong> - Sélectionne le premier (dernier) élément d'un type spécifique parmi ses frères.</li>
    </ul>
</div>



<h3>Le pseudo-class :first-child</h3>
<div style="float: left; width: 50%; font-size:70%;">
    <strong>HTML</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<div>
    <h2>Utilisation de :first-child</h2>
    <p>Le pseudo-class :first-child sélectionne l'élément qui est le premier enfant de son parent.</p>
    <div>Div en tant que deuxième élément - non ciblé</div>
    <p>Paragraphe 1 - non ciblé car ce n'est pas le premier enfant.</p>
    <section>
        <p>Paragraphe 2 dans le section - ciblé car c'est le premier enfant de la section.</p>
    </section>
    <p>Paragraphe 3 - non ciblé</p>
</div>
    </script></code></pre>
    <br>
    <strong>CSS</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<style>
p:first-child {
    background-color: lightblue;
}
</style>
    </script></code></pre>
</div>
<!-- Right side: Display the result -->
<div style="float: right; width: 50%;">
    <strong style="font-size:70%;">Résultat :</strong>
    <div style="font-size:0.5em; font-family: Arial, sans-serif; background-color: lightgrey; margin: 10px;">
        <h2>Utilisation de :first-child</h2>
        <p>Le pseudo-class :first-child sélectionne l'élément qui est le premier enfant de son parent.</p>
        <div>Div en tant que deuxième élément - non ciblé</div>
        <p>Paragraphe 1 - non ciblé car ce n'est pas le premier enfant.</p>
        <section>
            <p style="background-color: lightblue;">Paragraphe 2 dans le section - ciblé car c'est le premier enfant de la section.</p>
        </section>
        <p>Paragraphe 3 - non ciblé</p>
    </div>
</div>
<div style="clear:both;"></div>
<p><a href="https://www.w3schools.com/cssref/sel_firstchild.php"> En savoir plus sur :first-child sur W3Schools</a></p>


### Quelques exemples
[Sur codepen](https://codepen.io/knuxv/pen/dywmywg)  
[Sur W3schools](https://www.w3schools.com/cssref/trysel.php)  



### [Éléments Sémantiques en HTML](https://www.w3schools.com/html/html5_semantic_elements.asp)

<p style="font-size:70%;"><a href="https://www.w3schools.com/html/html5_semantic_elements.asp">Les éléments sémantiques (voir W3schools)</a> sont des éléments qui décrivent clairement leur signification à la fois pour le navigateur et le développeur.</p>

- **header**: En-tête du document ou d'une section.
- **nav**: Groupe de liens de navigation.
- **main**: Contenu principal unique du document.
- **article**: Contenu indépendant et autonome.
- **section**: Section distincte du contenu ou de la mise en page.
- **aside**: Contenu tangentiellement lié au contenu principal.
- **footer**: Pied de page du document ou d'une section.

> Note: S'utilise comme un `div`.



<h3>Table HTML</h3>
<div style="float: left; width: 50%; font-size:70%;">         
    <pre><code data-trim data-noescape><script type="text/template">
<table>
    <tr>
        <th>Titre du Livre</th>
        <th>Auteur</th>
        <th>Date de Publication</th>
        <th>Nombre d'Emprunts</th>
    </tr>
    <tr>
        <td>Les Misérables</td>
        <td>Victor Hugo</td>
        <td>1862</td>
        <td>120</td>
    </tr>
    <!-- More rows can be added here -->
</table>
   </script></code></pre> 
</div>
<!-- Right Side: Result Display -->
<div style="float: right; width: 50%;">
    <strong style="font-size:70%;">Résultat :</strong>
    <div style="font-size:0.5em; font-family: Arial, sans-serif; background-color: lightgrey; margin: 10px;">
        <table>
            <tr>
                <th>Titre du Livre</th>
                <th>Auteur</th>
                <th>Date de Publication</th>
                <th>Nombre d'Emprunts</th>
            </tr>
            <tr>
                <td>Les Misérables</td>
                <td>Victor Hugo</td>
                <td>1862</td>
                <td>120</td>
            </tr>
            <!-- The styled table will appear here -->
        </table>
    </div>
</div>

<div style="clear:both;"></div>

<p><a href="https://www.w3schools.com/html/html_tables.asp"> Lire davantage sur les tables HTML sur W3Schools</a></p>