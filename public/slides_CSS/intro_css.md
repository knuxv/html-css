### CSS (Cascading Style Sheet)
- Langage de feuille de style
- <u>Propriétés</u> et <u>attributs</u> pour définir des <b>styles</b>
- Encodage de balisage:   
<pre><code><p style="color:red; text-align:center">paragraphe ici.</p></code></pre>
<p style="color:red; text-align:center">paragraphe ici.</p>
- <strong>CSS</strong>:  Définit le style des éléments dans les balises. (police, couleur, taille, marges, etc...)
- <strong>Cascade</strong>: Les règles s'appliquent pour les balises dans les balises.



<h3 style="margin-top: -20%;">HTML : Encapsuler l'info</h3>
<div style="float: left; width: 50%">
    <ul>
        <li>Les balises HTML structurent notre contenu.</li>
        <br>
        <li>Une meilleure structure facilite l'accès et la présentation.</li>
    </ul>
</div>
<div style="float: right; width: 50%">
    <img src="slides_CSS/img/wraping.png" alt="Image d'encapsulation">
    <img src="slides_CSS/img/wraping.png" alt="Image d'encapsulation">
    <ul style="font-size:0.7em;">
        <li><strong>div</strong> : Bloc conteneur pour contenu.</li>
        <li><strong>span</strong> : Conteneur en ligne pour un texte spécifique.</li>
    </ul>
</div>



<h3 style="margin-top: -20%;">HTML : étiquetage correct</h3>

<div style="float: left; width: 50%;">
    <strong>Non :-(</strong>
    <pre><code data-noescape><script type="text/template">
<div>
Titre
Voici du contenu
Voici plus de contenu
Ce MOT en bleu
</div>
    </script></code></pre>
</div>

<div style="float: right; width: 50%;">
    <strong>Oui :)</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<div>
<h1>Titre</h1>
<p>Voici du contenu.</p>
<p>Voici plus de contenu</p>
<p>Ce<span>MOT</span> en bleu</p>
</div>
    </script></code></pre>
</div>

<div style="clear:both;"></div>




<h3 style="margin-top: -20%;">Titre HTML : bon usage</h3>
<p>2 <strong>sélecteurs</strong>, <u>Id</u> et <u>class</u>.</p>
<p><u>Attributs</u> de la balise.</p>
<ul>
    <li><strong>Id</strong>: donne un identifiant <u>unique</u> à une balise</li>
    <li><strong>class</strong>: donne un identifiant <u>générique</u> à une balise</li>
</ul>
<pre><code data-trim data-noescape>
<script type="text/template">
<div id="profile-picture" class="mini-image">...</div>
</script>
</code></pre>
<p>Ces sélecteurs nous permettent ensuite d'accéder à un contenu précis pour appliquer le CSS.</p>



<h3 style="margin-top: -10%;">Exemple CSS</h3>

<!-- Left top: Small HTML example -->
<div style="float: left; width: 50%;">
    <strong>Exemple de code HTML :</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<div>
    <h2>Titre de l'article</h2>
    <p>le premier paragraphe.</p>
    <p>le second paragraphe.</p>
</div>
    </script></code></pre>

    
<strong>Code CSS :</strong>
<pre><code data-trim data-noescape><script type="text/template">
* {
    color: blue;
    font: 14px Tahoma;
}

p {
    border: 1px solid black;
    padding: 10px;
    color:red;
}
    </script></code></pre>
</div>

<!-- Right side -->
<div style="float: right; width: 50%;">
    <!-- Right TOP: Display the result -->
    <strong>Résultat :</strong>
    <div style="color: blue; margin: 10px; font: 14px Tahoma;">
        <h2 style="color: blue;">Titre de l'article</h2>
        <p style="border: 1px solid black; padding: 10px;color:red;">Voici le premier paragraphe.</p>
        <p style="border: 1px solid black; padding: 10px;color:red;">Et voilà le second paragraphe.</p>
    </div>
<br>
<ul style="font-size:0.6em;">
    <li>Ceci modifiera <strong>toutes</strong> les balises de mon site web (‘*’ signifie "all").</li>
    <br>
    <li>Mais le CSS dans p {} ne s'applique qu'aux éléments <code>p</code>.</li>
</ul>
</div>

<div style="clear:both;"></div>



<h3 style="margin-top: -10%;">Ajout de règles CSS à votre site</h3>
<p><strong>3 méthodes</strong></p>
<ol>
    <li>Dans une balise <code>style</code> dans le <code>head</code></li>
<pre><code data-trim data-noescape><script type="text/template">
<style>
    p { color: blue }
</style>
</script></code></pre>

<li>Dans un fichier CSS externe</li>
<pre><code data-trim data-noescape><script type="text/template">
<link href="style.css" rel="stylesheet" />
</script></code></pre>

<li>Utiliser l'attribut <code>style</code> directement dans une balise</li>
<pre><code data-trim data-noescape><script type="text/template">
<p style="color: blue; margin: 10px">
</script></code></pre>
</ol>


<h3 style="margin-top: -10%;">Exemple d'un fichier HTML avec CSS dans le <code>head</code></h3>
<div style="font-size:0.55em;">
    <pre><code data-trim data-noescape><script type="text/template">
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exemple</title>
        <style>
            div {
                border: 1px solid black;
                padding: 15px;
            }
            h2 {
                color: blue;
            }
            p {
                color: green;
            }
        </style>
    </head>
    <body>
        <div>
            <h2>Titre de l'article</h2>
            <p>le premier paragraphe.</p>
            <p>le second paragraphe.</p>
        </div>
    </body>
    </html>
    </script></code></pre>
</div>


<h3 style="margin-top: -10%;">CSS externe</h3>
<div style="font-size: 0.7em;">
<pre><code data-trim data-noescape><script type="text/template">
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exemple</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <div>
        <h2>Titre de l'article</h2>
        <p>le premier paragraphe.</p>
        <p>le second paragraphe.</p>
    </div>
</body>
</html>
</script></code></pre>
</div>
<p>Dans cet exemple, le style est défini dans un fichier externe nommé <code>style.css</code>.</p>
<p> Utiliser, la balise <strong>link</strong> avec les attributs <u>href</u> et <u>rel</u>.

</p>



<h3 style="margin-top: -10%;">Exemple CSS avec classe et ID</h3>

<!-- Left top: Small HTML example with CSS in the head -->
<div style="float: left; width: 50%; font-size:70%;">
    <strong>HTML</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<div>
    <h3>Titre</h3>
    <p class="highlighted">Paragraphe avec class.</p>
    <p id="specialText">Texte avec ID.</p>
    <p id="specialText" class="highlighted">Texte avec class et ID.</p>
</div>
    </script></code></pre>
    <br>
    <strong>CSS</strong>
    <pre><code data-trim data-noescape><script type="text/template">
<style>
body {
    font-family: Arial, sans-serif;
    background-color: lightgrey;
    }
p {
    color: green;
}
.highlighted {
    background-color: yellow;
    text-transform: uppercase;
}
#specialText {
    font-weight: bold;
    color: red;
    background-color: green;
}
</style>
    </script></code></pre>
</div>

<!-- Right side -->
<div style="float: right; width: 50%;">
    <!-- Right TOP: Display the result -->
    <strong style="font-size:70%;">Résultat :</strong>
    <div style="font-family: Arial, sans-serif; background-color: lightgrey; margin: 10px;">
        <h3>Titre</h3>
<p style="color: green; background-color: yellow; text-transform: uppercase;">Paragraphe avec class.</p>
<p style="color: red; font-weight: bold; background-color: green;">Texte avec ID.</p>
<p style="color: red; font-weight: bold; background-color: green; text-transform: uppercase;">Texte avec class et ID.</p>
</div>
</div>
<div style="clear:both;"></div>
<p style="text-align:center"><u>L'ID a la priorité sur la classe.</u></p>



---
<h3 style="margin-top: -10%;">Définir la taille de la police en CSS</h3>
<p><strong>3 unités principales</strong></p>
<ul style="text-align: left; font-size:65%;">
    <li>
        <strong>px:</strong> Unité fixe qui détermine une taille précise.
    </li>
<pre><code data-trim data-noescape><script type="text/template">
<p style="font-size: 16px;">
    Ceci est du texte de taille 16px.
</p>
</script></code></pre>
    <li>
        <strong>em:</strong> Unité relative basée sur la taille de la police du parent. Par exemple, si le parent a une taille de police de 16px, alors <code>1em</code> serait égal à 16px.
    </li>
<pre><code data-trim data-noescape><script type="text/template">
<p style="font-size: 2em;">
    Ceci est du texte de taille 2em (32px si la taille de police du parent est 16px).
</p>
</script></code></pre>
    <li>
        <strong>%:</strong> Unité relative basée sur la taille de la police du parent. 100% serait la même taille que celle du parent.
    </li>
<pre><code data-trim data-noescape><script type="text/template">
<p style="font-size: 150%;">
    Ceci est du texte de taille 150% (24px si la taille de police du parent est 16px).
</p>
</script></code></pre>
</ul>


<h3 style="margin-top: -10%;">Utilisation des unités avec la propriété <code>border</code></h3>
<pre><code data-trim data-noescape><script type="text/template">
<p style="border: 2px solid black;">
    Ceci est un paragraphe avec une bordure de 2px.
</p>
</script></code></pre>

<p>Pour un guide détaillé sur les unités en CSS, <a href="https://www.w3schools.com/cssref/css_units.php">cliquez ici</a>.</p>



---
<h3 style="margin-top: -10%;">Définir les couleurs en CSS</h3>
<p><strong>2 méthodes principales</strong></p>
<ul style="text-align: left; font-size:65%;">
    <li>
        Avec le nom de la couleur en anglais:<br> 
        140 couleurs supportées: <a href="https://www.w3schools.com/colors/colors_names.asp">lien vers les couleurs</a>.
    </li>
<pre><code data-trim data-noescape><script type="text/template">
<p style="color: red;">
    Ceci est du texte en rouge.
</p>
</script></code></pre>
    <li>
    <strong>RGB:</strong> Utilisation de la fonction <code>rgb()</code> pour définir les couleurs en fonction des valeurs rouge, vert et bleu.
    </li>
<pre><code data-trim data-noescape><script type="text/template">
    <p style="background-color: rgb(255, 0, 0);">
        Ceci a un fond rouge.
    </p>
</script></code></pre>
</ul>

<p>Pour un guide détaillé sur les couleurs CSS, <a href="https://www.w3schools.com/css/css_colors.asp">cliquez ici</a>.</p>

---


<h3 style="margin-top: -10%;">Comprendre RGB en CSS</h3>

<p>En CSS, la fonction <code>rgb()</code> permet de définir des couleurs en utilisant des valeurs numériques pour les composants rouge, vert et bleu.</p>

<ul style="text-align: left; font-size:65%;">
    <li>
        Chaque composant (rouge, vert et bleu) peut avoir une valeur entre 0 et 255.
    </li>
    <li>
        Une valeur de <code>rgb(255, 0, 0)</code> représente la couleur rouge, car le composant rouge est à son maximum et les composants vert et bleu sont à zéro.
    </li>
    <li>
        De même, <code>rgb(0, 255, 0)</code> est vert et <code>rgb(0, 0, 255)</code> est bleu.
    </li>
    <li>
        En combinant ces trois composants, vous pouvez créer une large gamme de couleurs. Par exemple, <code>rgb(255, 255, 0)</code> serait du jaune.
    </li>
</ul>



<h3 style="margin-top: -15%; font-size:75%;">Comprendre RGB en CSS (2)</h3>
<img src="slides_CSS/img/RGBColor.jpg" alt="img_rgb" height=40%; width=40%; />
<br>
<a href="https://syncfiddle.net/fiddle/-NeYirA7jndjSjx4sDak">Testons ensemble.</a>
<br>
Expérimentez par vous-même sur <a href="https://codepen.io/knuxv/pen/KKbygGE">codepen</a>.


<h3 style="margin-top: -20%;">Découvrir les différentes balises HTML + CSS + Exercices</h3>
<ul>
    <li>
        <a href="https://www.w3schools.com/css/">Découvrez les différents éléments et styles CSS ici</a>
    </li>
    <li>
        <a href="https://www.w3schools.com/html/html_exercises.asp">Exercices HTML pour renforcer vos compétences</a>
    </li>
    <li>
        <a href="https://www.w3schools.com/css/css_exercises.asp">Exercices CSS pour maîtriser le style</a>
    </li>
</ul>
